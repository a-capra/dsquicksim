import pandas as pd
import matplotlib.pyplot as plt

df=pd.read_csv("./datareduction.csv",names=["S","npe","maxpe","ratio","raw","qt","raws","qts"])
ax=df[df["S"]=="S2"].plot.scatter(x="npe",y="ratio")
ax.set_xscale('log')
ax.grid(True)
ax.set_title("S2 data reduction")
plt.show()
